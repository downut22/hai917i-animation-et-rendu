package geo;

import java.awt.Graphics2D;
import java.awt.Point;
import java.awt.Rectangle;
import java.awt.geom.Point2D;
import java.io.File;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import javax.swing.JOptionPane;
import ui.Style;
import ui.config.Parameters;
import ui.io.ReadWritePoint;

public class Geste {
	private ArrayList<PointVisible> points ;
	private Style style = new Style();

	public Geste() {
		points = new ArrayList<PointVisible>();
	}
	
	public Geste(String fileName) {
		this();
		ReadWritePoint rwp = new ReadWritePoint(new File(fileName));
		points = rwp.read();
	}
	
	public Geste(Style style2) {
		this();
		style = style2;
	}

	public void add(Point p) {
		add(new PointVisible(p.x, p.y));	
	}
	
	public void add(PointVisible p) {
		points.add(p);	
	}
	
	public void drawPoints(Graphics2D g) {
		for (PointVisible p: points) {
			p.dessine(g, style);
		}
	}

	public void drawFeatures(Graphics2D g) {
		Rectangle r = computeBoundingBox();
		String features = points.size() + " points,  length ~> "+ Math.round(computeLength());
		g.translate(-r.x, -r.y);
		g.scale(2, 2);
		g.drawString(features,r.x, r.y - 10);
		g.scale(.5, .5);
		g.translate(r.x, r.y);
	}
	
	public void draw(Graphics2D g) {
		if (style.drawLine()) {
			drawLines(g);
		}
		if (style.drawPoints()) {
			drawPoints(g);
		}
		drawFeatures(g);	
	}

	private void drawLines(Graphics2D g) {
		PointVisible p1, p2;
		for (int i = 0; i < points.size()-1;i++) {
			p1 = points.get(i);
			p2 = points.get(i+1);
			g.drawLine(p1.x, p1.y, p2.x, p2.y);
		}
	}
	
	public Rectangle computeBoundingBox() {
		int minx, miny,maxx, maxy;
		minx = points.get(0).x;
		maxx = points.get(0).x;
		miny = points.get(0).y;
		maxy = points.get(0).y;
		for (PointVisible p: points) {
			if(p.x < minx) minx = p.x;
			if(p.y < miny) miny = p.y;
			if(p.x > maxx) maxx = p.x;
			if(p.y > maxy) maxy = p.y;
		}
		return new Rectangle(minx,miny,maxx-minx,maxy-miny);
	}
	
	public Geste rotate(double angle) {
		Geste rotatedGeste = new Geste();
		double sin = Math.sin(angle);
		double cos = Math.cos(angle);
		double x , y ;
		for (PointVisible p: points) {
			x = (double) p.x;
			y = (double) p.y;
			x = cos * x - sin * y;
			y = sin * x + cos * y;
			rotatedGeste.add(new PointVisible((int)x,(int)y));
		}
		return rotatedGeste;
	}
	
	public void exportWhenPossible(String filePath) {
		Path p = Paths.get(filePath);
		if (Files.exists(p)) {
			JOptionPane.showMessageDialog(null, "Error:"+p.getFileName()+", file exists, no overwrite, choose an empty directory");
		}else {
			export(filePath);
		}
	}
	
	private void export(String path) {
		ReadWritePoint rw = new ReadWritePoint(new File(path));
		for (PointVisible p: points){
			rw.add((int)p.x+";"+(int)p.y+";"+p.toString());
		}
		rw.write();
	}
	
	//Todo	
		//Calculer la longueur du geste (de la polyligne du tracé)
		private float computeLength()
		{
			float distance = 0;
			for(int i = 0; i < points.size()-1;i++)
			{
				double d = points.get(i).distance(points.get(i+1));
				distance += (float)d;
			}
			return distance;
		}

		//Creer un geste ré-échantillonné au nombre de points utiles pour l'algo OneDollarRecognizer
		public Geste oResample() {
			int n = Parameters.OneDollarSampleSize;
			float step = computeLength() / (n-1);
			Geste newGeste = new Geste();

			newGeste.add(points.get(0));

			float currentLength = 0;
			for(int i = 1; i < points.size();i++)
			{
				double d = points.get(i).distance(points.get(i-1));

				if(currentLength + d >= step)
				{
					double ratio = (step-currentLength) / d;
					double x = points.get(i-1).x + ((points.get(i).x - points.get(i-1).x) * ratio);
					double y = points.get(i-1).y + ((points.get(i).y - points.get(i-1).y) * ratio);
					PointVisible p = new PointVisible(x,y);
					newGeste.add(p);
					points.add(i,p);
					currentLength = 0;
				}
				else{currentLength += d;}
			}

			//newGeste.add(points.get(points.size()-1));

			return newGeste; // à modifier...
		}
		
		//Calculer le centre de gravité et le sauvegarder
		public Point2D.Double updateGravity() {
			Point2D.Double center = new Point2D.Double(0,0);

			for(int i = 0; i < points.size();i++)
			{
				center.x += points.get(i).x;
				center.y += points.get(i).y;
			}
			center.x /= points.size();
			center.y /= points.size();

			return center;
		}

		public double angle()
		{
			Point2D.Double center = updateGravity();
			PointVisible first = points.get(0);

			return Math.atan((center.y - first.y) / (center.x - first.x));
		}
		
		//Creer un geste avec une orientation à alpha
		public Geste oRotateTo(double alpha) {
			Geste newGeste = new Geste();
			Point2D.Double center = updateGravity();
			for(int i = 0; i < points.size();i++)
			{
				double x = ((points.get(i).x - center.x) * Math.cos(alpha)) - ((points.get(i).y - center.y) * Math.sin(alpha)) + center.x;
				double y = ((points.get(i).x - center.x) * Math.sin(alpha)) + ((points.get(i).y - center.y) * Math.cos(alpha)) + center.y;
				PointVisible p = new PointVisible(x,y);
				newGeste.add(p);
			}
			return newGeste; // à modifier...
		}

		public Geste rotatoTo0()
		{
			return oRotateTo(-angle());
		}
		
		//Creer un geste redimensionné 
		public Geste oRescale() {
			Geste newGeste = new Geste();

			Point2D.Double center = updateGravity() ;

			double bbminx = 10000,bbminy= 10000,bbmaxx= -10000,bbmaxy= -10000;
			for(int i = 0; i < points.size();i++)
			{
				bbminx = Math.min(bbminx,points.get(i).x - center.x);
				bbminy = Math.min(bbminy,points.get(i).y - center.y);
				bbmaxx = Math.max(bbmaxx,points.get(i).x - center.x);
				bbmaxy = Math.max(bbmaxy,points.get(i).y - center.y);
			}
			double width = bbmaxx - bbminx;
			double height = bbmaxy - bbminy;
			double size = Math.min(width,height);
			for(int i = 0; i < points.size();i++)
			{
				double x = (points.get(i).x - center.x) * (size / width) + center.x;
				double y = (points.get(i).y - center.y) * (size / height) + center.y;
				newGeste.add(new PointVisible(x,y));
			}

			return newGeste; // à modifier...
		}
		
		//Creer un geste recentré sur le centre de gravité 
		public Geste oRecenter() {
			Geste newGeste = new Geste();
			Point2D.Double center = updateGravity() ;

			for(int i = 0; i < points.size();i++)
			{
				double x = (points.get(i).x - center.x) ;
				double y = (points.get(i).y - center.y) ;
				newGeste.add(new PointVisible(x,y));
			}

			return newGeste; // à modifier...
		}
}
