
// -------------------------------------------
// gMini : a minimal OpenGL/GLUT application
// for 3D graphics.
// Copyright (C) 2006-2008 Tamy Boubekeur
// All rights reserved.
// -------------------------------------------

// -------------------------------------------
// Disclaimer: this code is dirty in the
// meaning that there is no attention paid to
// proper class attribute access, memory
// management or optimisation of any kind. It
// is designed for quick-and-dirty testing
// purpose.
// -------------------------------------------

#include <iostream>
#include <fstream>
#include <vector>
#include <algorithm>
#include <string>
#include <cstdio>
#include <cstdlib>

#include <algorithm>
#include <GL/glut.h>
#include <float.h>
#include "src/Vec3.h"
#include "src/Camera.h"
#include "src/jmkdtree.h"


using namespace std;

std::vector< Vec3 > positions;
std::vector< Vec3 > normals;

std::vector< Vec3 > positions2;
std::vector< Vec3 > normals2;


// -------------------------------------------
// OpenGL/GLUT application code.
// -------------------------------------------

static GLint window;
static unsigned int SCREENWIDTH = 640;
static unsigned int SCREENHEIGHT = 480;
static Camera camera;
static bool mouseRotatePressed = false;
static bool mouseMovePressed = false;
static bool mouseZoomPressed = false;
static int lastX=0, lastY=0, lastZoom=0;
static bool fullScreen = false;




// ------------------------------------------------------------------------------------------------------------
// i/o and some stuff
// ------------------------------------------------------------------------------------------------------------
void loadPN (const std::string & filename , std::vector< Vec3 > & o_positions , std::vector< Vec3 > & o_normals ) {
    unsigned int surfelSize = 6;
    FILE * in = fopen (filename.c_str (), "rb");
    if (in == NULL) {
        std::cout << filename << " is not a valid PN file." << std::endl;
        return;
    }
    size_t READ_BUFFER_SIZE = 1000; // for example...
    float * pn = new float[surfelSize*READ_BUFFER_SIZE];
    o_positions.clear ();
    o_normals.clear ();
    while (!feof (in)) {
        unsigned numOfPoints = fread (pn, 4, surfelSize*READ_BUFFER_SIZE, in);
        for (unsigned int i = 0; i < numOfPoints; i += surfelSize) {
            o_positions.push_back (Vec3 (pn[i], pn[i+1], pn[i+2]));
            o_normals.push_back (Vec3 (pn[i+3], pn[i+4], pn[i+5]));
        }

        if (numOfPoints < surfelSize*READ_BUFFER_SIZE) break;
    }
    fclose (in);
    delete [] pn;
}
void savePN (const std::string & filename , std::vector< Vec3 > const & o_positions , std::vector< Vec3 > const & o_normals ) {
    if ( o_positions.size() != o_normals.size() ) {
        std::cout << "The pointset you are trying to save does not contain the same number of points and normals." << std::endl;
        return;
    }
    FILE * outfile = fopen (filename.c_str (), "wb");
    if (outfile == NULL) {
        std::cout << filename << " is not a valid PN file." << std::endl;
        return;
    }
    for(unsigned int pIt = 0 ; pIt < o_positions.size() ; ++pIt) {
        fwrite (&(o_positions[pIt]) , sizeof(float), 3, outfile);
        fwrite (&(o_normals[pIt]) , sizeof(float), 3, outfile);
    }
    fclose (outfile);
}
void scaleAndCenter( std::vector< Vec3 > & io_positions ) {
    Vec3 bboxMin( FLT_MAX , FLT_MAX , FLT_MAX );
    Vec3 bboxMax( FLT_MIN , FLT_MIN , FLT_MIN );
    for(unsigned int pIt = 0 ; pIt < io_positions.size() ; ++pIt) {
        for( unsigned int coord = 0 ; coord < 3 ; ++coord ) {
            bboxMin[coord] = std::min<float>( bboxMin[coord] , io_positions[pIt][coord] );
            bboxMax[coord] = std::max<float>( bboxMax[coord] , io_positions[pIt][coord] );
        }
    }
    Vec3 bboxCenter = (bboxMin + bboxMax) / 2.f;
    float bboxLongestAxis = std::max<float>( bboxMax[0]-bboxMin[0] , std::max<float>( bboxMax[1]-bboxMin[1] , bboxMax[2]-bboxMin[2] ) );
    for(unsigned int pIt = 0 ; pIt < io_positions.size() ; ++pIt) {
        io_positions[pIt] = (io_positions[pIt] - bboxCenter) / bboxLongestAxis;
    }
}

void applyRandomRigidTransformation( std::vector< Vec3 > & io_positions , std::vector< Vec3 > & io_normals ) {
    srand(time(NULL));
    Mat3 R = Mat3::RandRotation();
    Vec3 t = Vec3::Rand(1.f);
    for(unsigned int pIt = 0 ; pIt < io_positions.size() ; ++pIt) {
        io_positions[pIt] = R * io_positions[pIt] + t;
        io_normals[pIt] = R * io_normals[pIt];
    }
}

void subsample( std::vector< Vec3 > & i_positions , std::vector< Vec3 > & i_normals , float minimumAmount = 0.1f , float maximumAmount = 0.2f ) {
    std::vector< Vec3 > newPos , newNormals;
    std::vector< unsigned int > indices(i_positions.size());
    for( unsigned int i = 0 ; i < indices.size() ; ++i ) indices[i] = i;
    srand(time(NULL));
    std::random_shuffle(indices.begin() , indices.end());
    unsigned int newSize = indices.size() * (minimumAmount + (maximumAmount-minimumAmount)*(float)(rand()) / (float)(RAND_MAX));
    newPos.resize( newSize );
    newNormals.resize( newSize );
    for( unsigned int i = 0 ; i < newPos.size() ; ++i ) {
        newPos[i] = i_positions[ indices[i] ];
        newNormals[i] = i_normals[ indices[i] ];
    }
    i_positions = newPos;
    i_normals = newNormals;
}

bool save( const std::string & filename , std::vector< Vec3 > & vertices , std::vector< unsigned int > & triangles ) {
    std::ofstream myfile;
    myfile.open(filename.c_str());
    if (!myfile.is_open()) {
        std::cout << filename << " cannot be opened" << std::endl;
        return false;
    }

    myfile << "OFF" << std::endl;

    unsigned int n_vertices = vertices.size() , n_triangles = triangles.size()/3;
    myfile << n_vertices << " " << n_triangles << " 0" << std::endl;

    for( unsigned int v = 0 ; v < n_vertices ; ++v ) {
        myfile << vertices[v][0] << " " << vertices[v][1] << " " << vertices[v][2] << std::endl;
    }
    for( unsigned int f = 0 ; f < n_triangles ; ++f ) {
        myfile << 3 << " " << triangles[3*f] << " " << triangles[3*f+1] << " " << triangles[3*f+2];
        myfile << std::endl;
    }
    myfile.close();
    return true;
}



// ------------------------------------------------------------------------------------------------------------
// rendering.
// ------------------------------------------------------------------------------------------------------------




void initLight () {
    GLfloat light_position1[4] = {22.0f, 16.0f, 50.0f, 0.0f};
    GLfloat direction1[3] = {-52.0f,-16.0f,-50.0f};
    GLfloat color1[4] = {1.0f, 1.0f, 1.0f, 1.0f};
    GLfloat ambient[4] = {0.3f, 0.3f, 0.3f, 0.5f};

    glLightfv (GL_LIGHT1, GL_POSITION, light_position1);
    glLightfv (GL_LIGHT1, GL_SPOT_DIRECTION, direction1);
    glLightfv (GL_LIGHT1, GL_DIFFUSE, color1);
    glLightfv (GL_LIGHT1, GL_SPECULAR, color1);
    glLightModelfv (GL_LIGHT_MODEL_AMBIENT, ambient);
    glEnable (GL_LIGHT1);
    glEnable (GL_LIGHTING);
}

void init () {
    camera.resize (SCREENWIDTH, SCREENHEIGHT);
    initLight ();
    //glCullFace (GL_BACK);
    //glEnable (GL_CULL_FACE);
    glDepthFunc (GL_LESS);
    glEnable (GL_DEPTH_TEST);
    glClearColor (0.2f, 0.2f, 0.3f, 1.0f);
    glEnable(GL_COLOR_MATERIAL);
}

vector<Vec3> meshVertices;
vector<unsigned int> meshTriangles;

void drawTriangleMesh( std::vector< Vec3 > const & i_positions , std::vector< unsigned int > const & i_triangles ) {
    glBegin(GL_TRIANGLES);
    
  	/*for(unsigned int tIt = 0 ; tIt < i_triangles.size() / 3 ; ++tIt) 
  	{
        Vec3 p0 = i_positions[3*tIt];
        Vec3 p1 = i_positions[3*tIt+1];
        Vec3 p2 = i_positions[3*tIt+2];
        Vec3 n = Vec3::cross(p1-p0 , p2-p0);
        n.normalize();
        glNormal3f( n[0] , n[1] , n[2] );
        glVertex3f( p0[0] , p0[1] , p0[2] );
        glVertex3f( p1[0] , p1[1] , p1[2] );
        glVertex3f( p2[0] , p2[1] , p2[2] );
    }*/
    for(int i = 0; i < i_triangles.size() - 2; i+=3)
    {
    		//cout << i_triangles[i] << " " << i_triangles[i+1] << "  " << i_triangles[i+2] << " " << i_positions.size() << endl;
    	   Vec3 p0 = i_positions[i_triangles[i]];
        Vec3 p1 = i_positions[i_triangles[i+1]];
        Vec3 p2 = i_positions[i_triangles[i+2]];
        Vec3 n = Vec3::cross(p1-p0 , p2-p0);
        n.normalize();
        glNormal3f( n[0] , n[1] , n[2] );
        glVertex3f( p0[0] , p0[1] , p0[2] );
        glVertex3f( p1[0] , p1[1] , p1[2] );
        glVertex3f( p2[0] , p2[1] , p2[2] );
    }

    glEnd();
}

void drawPointSet( std::vector< Vec3 > const & i_positions , std::vector< Vec3 > const & i_normals ) {
    glBegin(GL_POINTS);
    for(unsigned int pIt = 0 ; pIt < i_positions.size() ; ++pIt) {
        glNormal3f( i_normals[pIt][0] , i_normals[pIt][1] , i_normals[pIt][2] );
        glVertex3f( i_positions[pIt][0] , i_positions[pIt][1] , i_positions[pIt][2] );
    }
    glEnd();
}




struct VoxelPoint
{
	Vec3 position;
	float value;
};

struct VoxelCell
{
	VoxelPoint* points[8];
	Vec3 center;
	bool isEdge;
	int verticeIndex;
	
	VoxelCell(VoxelPoint** ps)
	{
		center = Vec3(0,0,0);
		for(int i = 0; i < 8 ; i++)
		{
			points[i] = ps[i];
			center += ps[i]->position;
		}
		center = center / 8.f;
	}
};

vector<VoxelPoint*> voxelPoints;
vector<VoxelCell*> voxelCells;
BasicANNkdTree kdtree;

void drawVoxelPoints(vector<VoxelPoint*> vps)
{
	glBegin(GL_POINTS);
    for(VoxelPoint* vp : vps)
    {
    		Vec3 p = vp->position;
    		if(vp->value < 0)
    		{ 
		   glNormal3f( p[0] ,p[1] , p[2] );
		   glVertex3f( p[0] , p[1] , p[2] );
		}
    }
    glEnd();
}

void drawVoxelCells(vector<VoxelCell*> vcs)
{
	glBegin(GL_POINTS);
    for(VoxelCell* vc : vcs)
    {
    		Vec3 p = vc->center;
    		if(vc->isEdge)
    		{ 
		   glNormal3f( p[0] ,p[1] , p[2] );
		   glVertex3f( p[0] , p[1] , p[2] );
		}
    }
    glEnd();
}

bool drawModel;
bool drawProjection;
bool drawVertices;
bool drawMesh;
bool drawVoxels;
bool drawCells;

void draw () {
    glPointSize(2); // for example...

    if(drawModel)
    {
        glColor3f(0.8,0.8,1);
        drawPointSet(positions , normals);
    }

    if(drawProjection)
    {
        glColor3f(1,0.5,0.5);
        drawPointSet(positions2 , normals2);
    }
    
    if(drawVertices)
    {
    	glPointSize(1);
    	 	glColor3f(0.5,1,0.9);
    	  drawPointSet(meshVertices,meshVertices);
    }
    
    if(drawVoxels)
    {
     glPointSize(2);glColor3f(0.5,1,0.5);
    	drawVoxelPoints(voxelPoints);
    }
    
    if(drawCells)
    {
    glPointSize(4);glColor3f(0.5,1,0.5);
    	drawVoxelCells(voxelCells);
    }
    
    if(drawMesh)
    {
    	drawTriangleMesh(meshVertices,meshTriangles);
    }	
}








void display () {
    glLoadIdentity ();
    glClear (GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    camera.apply ();
    draw ();
    glFlush ();
    glutSwapBuffers ();
}

void idle () {
    glutPostRedisplay ();
}

void key (unsigned char keyPressed, int x, int y) {
    switch (keyPressed) {
    case 'f':
        if (fullScreen == true) {
            glutReshapeWindow (SCREENWIDTH, SCREENHEIGHT);
            fullScreen = false;
        } else {
            glutFullScreen ();
            fullScreen = true;
        }
        break;

    case 'w':
        GLint polygonMode[2];
        glGetIntegerv(GL_POLYGON_MODE, polygonMode);
        if(polygonMode[0] != GL_FILL)
            glPolygonMode (GL_FRONT_AND_BACK, GL_FILL);
        else
            glPolygonMode (GL_FRONT_AND_BACK, GL_LINE);
        break;

    default:
        break;
    }
    idle ();
}

void mouse (int button, int state, int x, int y) {
    if (state == GLUT_UP) {
        mouseMovePressed = false;
        mouseRotatePressed = false;
        mouseZoomPressed = false;
    } else {
        if (button == GLUT_LEFT_BUTTON) {
            camera.beginRotate (x, y);
            mouseMovePressed = false;
            mouseRotatePressed = true;
            mouseZoomPressed = false;
        } else if (button == GLUT_RIGHT_BUTTON) {
            lastX = x;
            lastY = y;
            mouseMovePressed = true;
            mouseRotatePressed = false;
            mouseZoomPressed = false;
        } else if (button == GLUT_MIDDLE_BUTTON) {
            if (mouseZoomPressed == false) {
                lastZoom = y;
                mouseMovePressed = false;
                mouseRotatePressed = false;
                mouseZoomPressed = true;
            }
        }
    }
    idle ();
}

void motion (int x, int y) {
    if (mouseRotatePressed == true) {
        camera.rotate (x, y);
    }
    else if (mouseMovePressed == true) {
        camera.move ((x-lastX)/static_cast<float>(SCREENWIDTH), (lastY-y)/static_cast<float>(SCREENHEIGHT), 0.0);
        lastX = x;
        lastY = y;
    }
    else if (mouseZoomPressed == true) {
        camera.zoom (float (y-lastZoom)/SCREENHEIGHT);
        lastZoom = y;
    }
}


void reshape(int w, int h) {
    camera.resize (w, h);
}



Vec3 projectOnPlane(Vec3 inputPoint, Vec3 planePoint, Vec3 planeNormal)
{
	Vec3 dc = inputPoint - planePoint;
	float dot = Vec3::dot(dc,planeNormal);	
	Vec3 result = inputPoint - (dot * planeNormal);
	return result;
}

float singularWeight(float radius, float distance, float power)
{
	return pow(radius / distance,power);
}

float gaussWeight(float radius, float distance)
{
	float d = distance * distance;
	float r = radius * radius;
	return exp(-d / r);
}

float wendLand(float radius, float distance)
{
	float a = 1 - ( distance / radius);
	float b = 1 + (4 * (distance * radius));
	return pow(a,4) * b;
}

void processCentroid(ANNidxArray pointIds, ANNdistArray pointDistance, int k, float radius, int weightType, Vec3 & point, Vec3 & normal)
{
	point = Vec3(0,0,0);
	normal = Vec3(0,0,0);
	float counter = 0;
	for(int i = 0; i < k; ++i)
	{
		int id = pointIds[i];
		float weight = 
			weightType == 0 ? 	singularWeight(radius,pointDistance[i],2) :
			weightType == 1 ? 	gaussWeight(radius,pointDistance[i]) :
								wendLand(radius,pointDistance[i]);

		point += weight * positions[id];
		normal +=  weight * normals[id];

		counter += weight;
	}
	point /= counter;
	normal /= counter;
}

void HPSS(Vec3 inputPoint, 
	Vec3 & outputPoint, Vec3 & outputNormal, 
	vector<Vec3> const & positions, vector<Vec3> const & normals, BasicANNkdTree const & kdtree,
	int kernel_type, float radius, unsigned int nbIterations = 10, unsigned int knn = 20)
{
	for(int i = 0; i < nbIterations; ++i)
	{
		ANNidxArray nearest_ids = new ANNidx[knn];
	    ANNdistArray nearest_distances = new ANNdist[knn];
	    kdtree.knearest(inputPoint,knn,nearest_ids,nearest_distances);

	    Vec3 centroid, normal;
		processCentroid(nearest_ids,nearest_distances,knn,radius, kernel_type, centroid, normal);

		Vec3 project = projectOnPlane(inputPoint, centroid,normal);

		inputPoint = project;
		outputPoint = project;
		outputNormal = normal;
	}
}

void processCentroid2(ANNidxArray pointIds, ANNdistArray pointDistance, int k, float radius, int weightType, Vec3 inputPoint, Vec3 & point, Vec3 & normal)
{
	point = Vec3(0,0,0);
	normal = Vec3(0,0,0);
	float counter = 0;
	for(int i = 0; i < k; ++i)
	{
		int id = pointIds[i];
		float weight = 
			weightType == 0 ? 	singularWeight(radius,pointDistance[i],2) :
			weightType == 1 ? 	gaussWeight(radius,pointDistance[i]) :
								wendLand(radius,pointDistance[i]);

		Vec3 project = projectOnPlane(inputPoint, positions[id],normals[id]);

		point += weight * project;
		normal +=  weight * normals[id];

		counter += weight;
	}
	point /= counter;
	normal /= counter;
}

void HPSS2(Vec3 inputPoint, 
	Vec3 & outputPoint, Vec3 & outputNormal, 
	vector<Vec3> const & positions, vector<Vec3> const & normals, BasicANNkdTree const & kdtree,
	int kernel_type, unsigned int nbIterations = 10, unsigned int knn = 20, float noise = 0)
{
	for(int i = 0; i < nbIterations; ++i)
	{
		ANNidxArray nearest_ids = new ANNidx[knn];
	    ANNdistArray nearest_distances = new ANNdist[knn];
	    kdtree.knearest(inputPoint,knn,nearest_ids,nearest_distances);

	    float radius = 0;
	    for(int i = 0; i < knn; i++){radius = max(radius,(float)nearest_distances[i]);}

	    Vec3 centroid, normal;
		processCentroid2(nearest_ids,nearest_distances,knn,radius, kernel_type,inputPoint, centroid, normal);

		Vec3 project = projectOnPlane(inputPoint, centroid,normal);

        float n = -noise + (noise * 2 * (rand()/(double)(RAND_MAX)));
		outputPoint = project + (n * normal);
		outputNormal = normal;
        inputPoint = outputPoint;
	}
}


int kernelCount = 10,kernelType = 0,itCount = 10;


int resolution;
VoxelPoint* getVoxelPointAt(int x,int y, int z)
{	
	int i = x + (y*resolution) + (z*resolution*resolution);
	return voxelPoints[i];
}
float getValueAt(int x,int y, int z)
{
	return getVoxelPointAt(x,y,z)->value;
}

void evaluateGrid(vector<Vec3> modelPoints, vector<Vec3> modelNormals,BasicANNkdTree & kdtree)
{
	for(VoxelPoint* vp : voxelPoints)
	{
		Vec3 outputPoint, outputNormal;
		HPSS2(vp->position, outputPoint, outputNormal, modelPoints, modelNormals, kdtree,kernelType,itCount,kernelCount,0);
		
		Vec3 direction = vp->position - outputPoint;
		float scal = Vec3::dot(direction,outputNormal);
		vp->value = scal;
	}
}

void addTriangle(int v1,int v2, int v3)
{
	meshTriangles.push_back(voxelCells[v1]->verticeIndex);
	meshTriangles.push_back(voxelCells[v2]->verticeIndex);
	meshTriangles.push_back(voxelCells[v3]->verticeIndex);
	//cout << voxelCells[v1]->verticeIndex << "  " << voxelCells[v2]->verticeIndex << "  " << voxelCells[v3]->verticeIndex << endl;
}

void registerCell(int id)
{
	if(voxelCells[id]->verticeIndex < 0)
	{
		Vec3 outputPoint, outputNormal;
		HPSS2(voxelCells[id]->center, outputPoint, outputNormal, positions, normals, kdtree,kernelType,itCount,kernelCount,0);
		
		meshVertices.push_back(outputPoint);
		voxelCells[id]->verticeIndex =  meshVertices.size()-1;
	}
}

void generateMesh()
{
	for(VoxelCell* cell : voxelCells)
	{
		cell->verticeIndex = -1;
	}
	
	int cellCount = resolution-1;
	for(int z = 0; z < cellCount-1;z++)
	{
		for(int y = 0; y < cellCount-1;y++)
		{
			for(int x = 0; x < cellCount-1;x++)
			{
				int id0 = x + (y*cellCount) + (z*cellCount*cellCount);
				int id1 = (x+1) + (y*cellCount) + (z*cellCount*cellCount);
				int id2 = (x+1) + ((y+1)*cellCount) + (z*cellCount*cellCount);
				int id3 = x + ((y+1)*cellCount) + (z*cellCount*cellCount);
				
				VoxelPoint* edge0 = voxelCells[id0]->points[7];
				VoxelPoint* edge1 = voxelCells[id0]->points[6];
									
				if(voxelCells[id0]->isEdge || voxelCells[id1]->isEdge || voxelCells[id2]->isEdge || voxelCells[id3]->isEdge)
				{
					registerCell(id0); registerCell(id1);registerCell(id2); registerCell(id3);
					if(edge0->value > edge1->value)
					{
						addTriangle(id0,id1,id2);
						addTriangle(id2,id3,id0);
					}
					else if(edge0->value < edge1->value)
					{
						addTriangle(id2,id1,id0);
						addTriangle(id0,id3,id2);
					}
				}
			}
		}
	}
	
	for(int y = 0; y < cellCount-1;y++)
	{
		for(int z = 0; z < cellCount-1;z++)
		{
			for(int x = 0; x < cellCount-1;x++)
			{
				int id0 = x + (y*cellCount) + (z*cellCount*cellCount);
				int id1 = (x+1) + (y*cellCount) + (z*cellCount*cellCount);
				int id2 = (x+1) + (y*cellCount) + ((z+1)*cellCount*cellCount);
				int id3 = x + (y*cellCount) + ((z+1)*cellCount*cellCount);
				
				VoxelPoint* edge0 = voxelCells[id0]->points[2];
				VoxelPoint* edge1 = voxelCells[id0]->points[6];
									
				if(voxelCells[id0]->isEdge || voxelCells[id1]->isEdge || voxelCells[id2]->isEdge || voxelCells[id3]->isEdge)
				{
					registerCell(id0); registerCell(id1);registerCell(id2); registerCell(id3);
					if(edge0->value > edge1->value)
					{
						addTriangle(id0,id1,id2);
						addTriangle(id2,id3,id0);
					}
					else if(edge0->value < edge1->value)
					{
						addTriangle(id2,id1,id0);
						addTriangle(id0,id3,id2);
					}
				}
			}
		}
	}
	for(int x = 0; x < cellCount-1;x++)
	{
		for(int z = 0; z < cellCount-1;z++)
		{
			for(int y = 0; y < cellCount-1;y++)
			{		
				int id0 = x + (y*cellCount) + (z*cellCount*cellCount);
				int id1 = x + ((y+1)*cellCount) + (z*cellCount*cellCount);
				int id2 = x + ((y+1)*cellCount) + ((z+1)*cellCount*cellCount);
				int id3 = x + (y*cellCount) + ((z+1)*cellCount*cellCount);
				
				VoxelPoint* edge0 = voxelCells[id0]->points[5];
				VoxelPoint* edge1 = voxelCells[id0]->points[6];
									
				if(voxelCells[id0]->isEdge || voxelCells[id1]->isEdge || voxelCells[id2]->isEdge || voxelCells[id3]->isEdge)
				{
					registerCell(id0); registerCell(id1);registerCell(id2); registerCell(id3);
					if(edge0->value > edge1->value)
					{
						addTriangle(id0,id1,id2);
						addTriangle(id2,id3,id0);
					}
					else if(edge0->value < edge1->value)
					{
						addTriangle(id2,id1,id0);
						addTriangle(id0,id3,id2);
					}
				}
			}
		}
	}
}

void generateCells()
{
	voxelCells.clear();
	for(int x = 0; x < resolution-1;x++)
	{
		for(int y = 0; y < resolution-1;y++)
		{
			for(int z = 0; z < resolution-1;z++)
			{
				VoxelPoint** vps = new VoxelPoint*[8];
				vps[0] = getVoxelPointAt(x,y,z); vps[1] = getVoxelPointAt(x,y,z+1); vps[2] = getVoxelPointAt(x+1,y,z+1);vps[3] = getVoxelPointAt(x+1,y,z); 
				vps[4] = getVoxelPointAt(x,y+1,z); vps[5] = getVoxelPointAt(x,y+1,z+1);vps[6] = getVoxelPointAt(x+1,y+1,z+1); vps[7] = getVoxelPointAt(x+1,y+1,z);
				
				bool isEdge = false;
				for(int i = 0; i < 8 && !isEdge; i++)
				{
					for(int j = i+1; j < 8; j++)
					{
						if(vps[i]->value > 0 && vps[j]->value < 0){isEdge = true; break;}
						if(vps[i]->value < 0 && vps[j]->value > 0){isEdge = true; break;}
					}
				}
				
				VoxelCell* vc = new VoxelCell(vps);
				vc->isEdge = isEdge;
				voxelCells.push_back(vc);
			}
		}
	}
}

void generateGrid(vector<Vec3> modelPoints, Vec3 offset)
{
	Vec3 bbmin = Vec3(FLT_MAX,FLT_MAX,FLT_MAX); Vec3 bbmax = Vec3(FLT_MIN,FLT_MIN,FLT_MIN);
	for(Vec3 mp : modelPoints)
	{
		bbmin[0] = min(bbmin[0],mp[0]);
		bbmin[1] = min(bbmin[1],mp[1]);
		bbmin[2] = min(bbmin[2],mp[2]);
		bbmax[0] = max(bbmax[0],mp[0]);
		bbmax[1] = max(bbmax[1],mp[1]);
		bbmax[2] = max(bbmax[2],mp[2]);
	}
	
	bbmin -= offset;
	bbmax += offset;
	Vec3 bbv = bbmax - bbmin;
	
	for(int x = 0; x < resolution;x++)
	{
		for(int y = 0; y < resolution;y++)
		{
			for(int z = 0; z < resolution;z++)
			{
				float rx = bbmin[0] + (bbv[0] * (x/float(resolution-1)));
				float ry = bbmin[1] + (bbv[1] * (y/float(resolution-1)));
				float rz = bbmin[2] + (bbv[2] * (z/float(resolution-1)));
				
				VoxelPoint* vp = new VoxelPoint();
				vp->position = Vec3(rx,ry,rz);
				vp->value = 0;
				voxelPoints.push_back(vp);
			}
		}
	}
}

int main (int argc, char ** argv) {
    /*if (argc > 2) {
        exit (EXIT_FAILURE);
    }*/
    glutInit (&argc, argv);
    glutInitDisplayMode (GLUT_RGBA | GLUT_DEPTH | GLUT_DOUBLE);
    glutInitWindowSize (SCREENWIDTH, SCREENHEIGHT);
    window = glutCreateWindow ("tp point processing");

    init ();
    glutIdleFunc (idle);
    glutDisplayFunc (display);
    glutKeyboardFunc (key);
    glutReshapeFunc (reshape);
    glutMotionFunc (motion);
    glutMouseFunc (mouse);
    key ('?', 0, 0);

	

    {
        // Load a first pointset, and build a kd-tree:
        if(argc <= 1)
        {
        	loadPN("pointsets/igea.pn" , positions , normals);
        }
        else
        {
        	std::string path = std::string("pointsets/") + std::string(argv[1]);
        	loadPN(path, positions , normals);
        }

        if(argc >= 3)
        {
        	sscanf (argv[2],"%d",&kernelCount) ;
        }
        else
        {
        	kernelCount = 20;
        }
        if(argc >= 4)
        {
        	sscanf (argv[3],"%d",&kernelType) ;
        }
        else
        {
        	kernelType = 0;
        }
        if(argc >= 5)
        {
        	sscanf (argv[4],"%d",&itCount) ;
        }
        else
        {
        	kernelType = 1;
        }
    
        kdtree.build(positions);


		resolution = 50;
		generateGrid(positions,Vec3(0.05,0.05,0.05));
		evaluateGrid(positions,normals,kdtree);
		generateCells();
    	
    		positions2.clear(); 
    		//positions2.resize(voxelPoints.size());
    		for(VoxelPoint* vp : voxelPoints)
        	{
        		positions2.push_back(vp->position);
        	}
    		normals2.resize(positions2.size());
    
    		generateMesh();
    

        /*for(int pi = 0; pi < positions2.size(); ++pi)
        {
        	Vec3 inputPoint = positions2.at(pi);
        	Vec3 outputPoint,outputNormal;

        	//HPSS(inputPoint, outputPoint, outputNormal, positions, normals, kdtree,kernelType, kernelRadius,itCount);
        	HPSS2(inputPoint, outputPoint, outputNormal, positions, normals, kdtree,kernelType,itCount,kernelCount,noise);

        	positions2[pi] = outputPoint;
        	normals2[pi] = outputNormal;
        }*/


        drawModel = false;
        drawProjection = false;
        drawVertices = true;
        drawMesh = true;
        drawVoxels = false;
        drawCells = false;

        // PROJECT USING MLS (HPSS and APSS):
        // TODO
    }



    glutMainLoop ();
    return EXIT_SUCCESS;
}

